import FirstPage from "./pages/FirstPage";
import HomePage from "./pages/HomePage";
import SecondPage from "./pages/SecondPage";
import ThirdPage from "./pages/ThirdPage";

const routeList = [
    { path:"/", element:<HomePage/>},
    { label:"First Page", path:"/firstpage", element:<FirstPage/>},
    { label:"Second Page", path:"/secondpage", element:<SecondPage/>},
    { label:"Second Page with Param", path:"/secondpage/:param1/:param2", element:<SecondPage/>},
    { label:"Second Page with third Param", path:"/secondpage/:third", element:<SecondPage/>},
    { label:"Third Page", path:"/thirdpage", element:<ThirdPage/>},
]

export default routeList;