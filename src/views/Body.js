import { Route, Routes } from "react-router-dom";
import FirstPage from "../pages/FirstPage";
import HomePage from "../pages/HomePage";
import SecondPage from "../pages/SecondPage";
import ThirdPage from "../pages/ThirdPage";
import routeList from "../routes";

function Body() {
    return (
        <div>
            <Routes>
                {
                    routeList.map((route, index) => {
                        if (route.path) {
                            return <Route path={route.path} element={route.element}></Route>
                        } else {
                            return null
                        }
                    })
                }
                <Route path="*" element={<HomePage/>}></Route>                
            </Routes>        
        </div>
    )
}

export default Body;