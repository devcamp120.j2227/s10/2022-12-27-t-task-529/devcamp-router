import { useEffect } from "react";
import { useNavigate, useParams, useSearchParams } from "react-router-dom";

function SecondPage() {
    const {param1, param2} = useParams();
    const [param] = useSearchParams();
    const navigate = useNavigate();

    console.log(param1);
    console.log(param2);
    console.log(param.get("name"));
    console.log(param.get("age"));
    useEffect(()=> {
        if (param1 === "third") {
            navigate("/thirdpage");
        }
    })
    return (
        <div>
         {
            param1 ? `param of second page is : ${param1} and ${param2}` : "second page without param"  
         }
        </div>
    )
}

export default SecondPage;